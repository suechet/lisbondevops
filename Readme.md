This is a project for a workshop.

The build the application you should execute:

  Mac/Linux: `./gradlew clean build`
  Windows: `gradlew clean build`
    
To run the application you should execute:

  Mac/Linux: `./gradlew bootRun`
  Windows: `gradlew bootRun`
    
To run the unit tests you should execute:

  Mac/Linux: `./gradlew test`
  Windows: `gradlew test`

To runt he cucumber tests you should execute:

  Mac/Linux: `./gradlew intTest`
  Windows: `gradlew intTest`
